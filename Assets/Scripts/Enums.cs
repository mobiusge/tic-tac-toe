﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTac
{
    public enum Type
    {
        None,
        X,
        O
    }

    public enum Turn
    {
        Player1,
        Player2,
        None
    }
}
