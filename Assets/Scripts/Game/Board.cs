﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTac.Game
{
    public class Board
    {
        public Cell[][] Cels { get; set; }

        public Board()
        {
            Cels = new Cell[][]
            {
                new Cell[] { new Cell(), new Cell(), new Cell() },
                new Cell[] { new Cell(), new Cell(), new Cell() },
                new Cell[] { new Cell(), new Cell(), new Cell() }
            };
        }

        public bool IsFinnished
        {
            get
            {
                if (IsFull()) return true;
                if (Winner != Type.None) return true;
                return false;
            }
        }

        public Type Winner
        {
            get
            {
                for (int i = 0; i < Cels.Length; i++)
                {
                    if (Cels[i][0].Type != Type.None)
                    {
                        if (Cels[i][0].Type == Cels[i][1].Type && Cels[i][0].Type == Cels[i][2].Type) return Cels[i][0].Type;
                    }
                }
                for (int i = 0; i < Cels.Length; i++)
                {
                    if (Cels[0][i].Type != Type.None)
                    {
                        if (Cels[0][i].Type == Cels[1][i].Type && Cels[0][i].Type == Cels[2][i].Type) return Cels[0][i].Type;
                    }
                }
                if (Cels[0][0].Type != Type.None)
                {
                    if (Cels[0][0].Type == Cels[1][1].Type && Cels[0][0].Type == Cels[2][2].Type) return Cels[0][0].Type;
                }
                if (Cels[0][2].Type != Type.None)
                {
                    if (Cels[0][2].Type == Cels[1][1].Type && Cels[0][2].Type == Cels[2][0].Type) return Cels[0][2].Type;
                }
                return Type.None;
            }
        }

        private bool IsFull()
        {
            for (int i = 0; i < Cels.Length; i++)
            {
                for (int j = 0; j < Cels[i].Length; j++)
                {
                    if (Cels[i][j].Type == Type.None) return false;
                }
            }
            return true;
        }
    }

    public class Cell
    {
        public Type Type { get; set; }

        public Cell()
        {
            this.Type = Type.None;
        }

        public Cell(Type type)
        {
            this.Type = type;
        }
    }
}
