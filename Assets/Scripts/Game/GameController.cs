﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

namespace TicTac.Game
{
    public class GameController : MonoBehaviour
    {
        public GameObject panelGame;
        public GameObject panelFinnish;
        public Text labelWinner;
        public Turn turn;
        public string winnerText;
        public CellController[] cells;
        public GameObject[] crossLines;
        private Board board;

        // Use this for initialization
        void Start()
        {
            board = new Board();
            int count = 0;
            for (int i = 0; i < board.Cels.Length; i++)
            {
                for (int j = 0; j < board.Cels[i].Length; j++)
                {
                    cells[count].Init(board.Cels[i][j].Type, j, i, OnCellClick);
                    count++;
                }
            }
        }

        private void OnCellClick(CellController cell)
        {
            MakeTurn(cell);
            CheckForWinner();
        }

        private void MakeTurn(CellController cell)
        {
            if (turn == Turn.Player1)
            {
                cell.MakeTurn(Type.X);
                board.Cels[cell.corX][cell.corY].Type = Type.X;
            }
            else
            {
                cell.MakeTurn(Type.O);
                board.Cels[cell.corX][cell.corY].Type = Type.O;
            }
            SwitchTurn();
        }

        private void SwitchTurn()
        {
            if (turn == Turn.Player1) turn = Turn.Player2;
            else turn = Turn.Player1;
        }

        private void CheckForWinner()
        {
            if (board.IsFinnished)
            {
                FinnishGame(board.Winner);
            }
        }

        private void FinnishGame(Type winner)
        {
            if (winner != Type.None)
            {
                labelWinner.text = string.Format(winnerText, winner == Type.X ? "Player # 1" : "Player # 2");
            }
            else
            {
                labelWinner.text = string.Format("It's a Draw");
            }
            panelFinnish.SetActive(true);
        }

        public void ButtonRetry()
        {
            ActionRetry();
        }

        private void ActionRetry()
        {
            Application.LoadLevel(Application.loadedLevelName);
        }

        public void ButtonMenu()
        {
            ActionMenu();
        }

        private void ActionMenu()
        {
            throw new NotImplementedException();
        }
    }

}